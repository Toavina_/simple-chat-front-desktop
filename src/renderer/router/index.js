import VueRouter from 'vue-router'
import Register from './../components/pages/Register'
import Login from './../components/pages/Login'
import Chat from './../components/pages/chatroom/chat'
import Search from './../components/pages/chatroom/components/search'
import ChatBox from './../components/pages/chatroom/components/chatbox'
import Contacts from './../components/pages/chatroom/components/contacts'
import User from './../components/pages/chatroom/components/user'
import Invitation from './../components/pages/chatroom/components/invitation'
import Vue from 'vue'

Vue.use(VueRouter)

const routes = 
[
    {
        path: '/',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/chat',
        name: 'chat',
        component: Chat,
        children: [
            {
                  path: 'home',
                  name: 'chat_home',
                  component: Search,
            },
            {
                  path: ':id',
                  name: 'chat_box',
                  component: ChatBox,
            },
            {
                path: 'contacts',
                name: 'chat_contacts',
                component: Contacts,
            },
            {
                path: 'user/:id',
                name: 'chat_user',
                component: User
            },
            {
                path: 'invitation',
                name: 'chat_invitation',
                component: Invitation
            }
            
          ]
    }

]

   


const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
